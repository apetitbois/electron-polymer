# electron-template

**Clone and run for a quick way to build a base Electron with Polymer Application.**

This basic template will provide you with base [Polymer](https://www.polymer-project.org/) support.

A basic Electron application needs just these files:

- `polymer/index.html` - A web page to render.
- `main.js` - Starts the app and creates a browser window to render HTML.
- `package.json` - Points to the app's main file and lists its details and dependencies.

## To Use

To clone and run this repository you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# create the application folder
mkdir electron-polymer
# Go into the repository
cd electron-polymer
# Clone this repository
git clone <Repository>
# Install dependencies and run the app
npm install && npm start
```

Learn more about Electron and its API in the [documentation](http://electron.atom.io/docs/latest).

#### License [Apache License Version 2.0](LICENSE.md)
