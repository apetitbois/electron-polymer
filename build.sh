#!/bin/bash

build_darwin() {
  electron-packager . \
    --platform=darwin\
    --arch=x64\
    --asar\
    --version=0.37.5\
    --out=builds\
    --overwrite
}

cd polymer
bower update
cd elements;vulcanize -o elements.vulcanized.html elements.html --strip-comments --inline-scripts --inline-css
cd ..;cp bower_components/webcomponentsjs/webcomponents-lite.js scripts/webcomponents-lite.js
rm -rf bower_components
cd ..

build_darwin
